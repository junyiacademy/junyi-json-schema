const exercise = {
  type: "object",
  required: ["id", "title", "url", "progress", "isUpgradable", "lockUntil"],
  properties: {
    id: {
      type: "string",
      faker: "random.uuid",
    },
    title: {
      type: "string",
      faker: "random.word",
    },
    url: { type: "string", faker: "internet.url" },
    progress: {
      type: "string",
      enum: [
        "",
        "started",
        "level1",
        "level2",
        "level3",
        "started",
        "proficient",
        "struggling",
      ],
    },
    isUpgradable: { type: "boolean", faker: "random.boolean" },
    lockUntil: { type: "string", faker: "date.future" },
  },
};

module.exports = { exercise };
